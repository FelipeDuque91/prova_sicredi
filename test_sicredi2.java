package sicredi_test_group.maven;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class test_sicredi2 {
	
	String message;
	
	
	public static  void main(String[] args) 
	{
		
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
		
		Select comboBox = new Select(driver.findElement(By.id("switch-version-select")));
		comboBox.selectByVisibleText("Bootstrap V4 Theme");
		
		driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();
				
		WebElement name = driver.findElement(By.id("field-customerName"));
		name.sendKeys("Teste Sicredi");
		
		WebElement last_name = driver.findElement(By.id("field-contactLastName"));
		last_name.sendKeys("Teste");
		
		WebElement contact_first_name = driver.findElement(By.id("field-contactFirstName"));
		contact_first_name.sendKeys("Felipe Duque");
		
		WebElement phone = driver.findElement(By.id("field-phone"));
		phone.sendKeys("51 9999-9999");
		
		WebElement address_line1 = driver.findElement(By.id("field-addressLine1"));
		address_line1.sendKeys("Av Assis Brasil, 3970");
		
		WebElement address_line2 = driver.findElement(By.id("field-addressLine2"));
		address_line2.sendKeys("Torre D");
		
		WebElement city = driver.findElement(By.id("field-city"));
		city.sendKeys("Porto Alegre");
		
		WebElement state = driver.findElement(By.id("field-state"));
		state.sendKeys("RS");
		
		WebElement postal_code = driver.findElement(By.id("field-postalCode"));
		postal_code.sendKeys("91000-000");
		
		WebElement country = driver.findElement(By.id("field-country"));
		country.sendKeys("Brasil");
	
		WebElement sales_rep_emp = driver.findElement(By.id("field-salesRepEmployeeNumber"));
		sales_rep_emp.sendKeys("0000");
		//Não consegui o preenchimento do texto "Fixter", a class dele espera "numeric form-control", diferente dos outros.
		
		WebElement credit_limit = driver.findElement(By.id("field-creditLimit"));
		credit_limit.sendKeys("200");
		
		driver.findElement(By.id("form-button-save")).click();
			
		String testeAutomacao = driver.findElement(By.id("report-success")).getTagName();
		
		String expectedMessage = "div";
		
		Assert.assertEquals(testeAutomacao, expectedMessage);
		//O assert não ficou ideal, tive dificuldade em encontrar o valor correto, pelo fato da mensagem estar dividida em 3 partes,
		//tentei de muitas formas identificar a mensagem que o desafio pede, infelizmente não encontrei.
		
		System.out.print("Mensagem Validada");
		
		driver.quit();
	
	}
	

}
